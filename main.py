"""
Write code that starts at (0,0) and "walks" in an outward spiral.
"""
import matplotlib.pyplot as plt

TOTAL_STEPS = 999


def next_position(pos, direction):
    """Given position and direction, figure out next position."""
    if direction == 0:
        delta = (0, 1)
    elif direction == 1:
        delta = (1, 0)
    elif direction == 2:
        delta = (0, -1)
    else:
        delta = (-1, 0)

    return tuple(map(lambda i, j: i + j, pos, delta))


def position_and_direction_method(total_steps=100):
    """Using current position and "which way you are facing", determine a spiral path."""
    position = (0, 0)
    facing = 0
    pos_visited = [position, ]

    # Walk and take note of steps (in tuples)
    while total_steps:
        """If you can't go right then go straight."""
        test_facing = (facing + 1) % 4
        test_step = next_position(position, test_facing)
        if test_step not in pos_visited:
            # We haven't visited the block to the "right".  Go there.
            position = test_step
            facing = test_facing
        else:
            # Stepping to the "right" is blocked.  Step forward.
            position = next_position(position, facing)

        pos_visited.append(position)
        total_steps -= 1

    return pos_visited


def draw_path(positions):
    # Unpack tuples for plotting.
    x_list = []
    y_list = []
    for position in positions:
        x, y = position
        x_list.append(x)
        y_list.append(y)

    # Finally, plot the path.
    plt.plot(x_list, y_list)
    plt.show()


if __name__ == "__main__":
    path_walked = position_and_direction_method(total_steps=TOTAL_STEPS)

    draw_path(positions=path_walked)
