# Walk in a spiral

I wanted to write something more elegant (and maybe I'll come back to this) but I had to try.  My goal was to write a simple 
"if" list that would "walk" in a spiral.

It isn't elegant, but it will produce a "spiral" walking pattern.
